# YOLOv3 with Distance Estimation

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](LICENSE)

This repository is an extension of the original [TF Keras YOLOv4/v3/v2 Modelset (https://github.com/david8862/keras-YOLOv3-model-set)](https://github.com/david8862/keras-YOLOv3-model-set), but 
the YOLOv3 only is extended for the distance estimation. Everytime you need more information about the underlying backbones, command line 
arguments or anything similar, please look at the documentation of the original repository (previous link).

This repository presents YOLOv3 (with Xception backbone) extension for distance estimation. It is the implementation related to the paper 
[Dist-YOLO: Fast Object Detection with Distance Estimation (https://www.mdpi.com/2076-3417/12/3/1354#cite)](https://www.mdpi.com/2076-3417/12/3/1354#cite).

The basic usage is similar to the original repository.

**Note:** Only Xception as a backbone was tested for this implementation. To use other backbones, some slight code changes may be neccessary.

## Label preparation

For the training/evaluation process a slightly adjusted YOLOv3 label format data needs to be prepared. The distance information is 
added as the last integer value at the end of the bbox description. Note the distances are clipped into (0,150) range. Example of the original
YOLOv3 line. The original line is in the format `file boxinfo boxinfo boxinfo boxinfo ...` with spaces as separators. Every `boxinfo` consists 
of `x1,y1,x2,y2,cls` values (no spaces!), where the first four define upper-top/lower-bottom bounding box points, the last one defines class index, e.g.: 
`/kitty/data_object_image_2/training/image_2/000000.png 712,143,810,307,0`.

For distance, the additional distance value must be added to `boxinfo`, so the final result is `x1,y1,x2,y2,cls,dist`, e.g.: `kitty/data_object_image_2/training/image_2/000000.png 712,143,810,307,0,8`.

### Kitty YOLOv3 labels to download
For shortcut, we offer prepared labels for [Kitty dataset](http://www.cvlibs.net/datasets/kitti/eval_object.php?obj_benchmark=3d) as ZIP file. 
The labels without distances are available [here](https://owncloud.cesnet.cz/index.php/s/rpnwsCOKsdv14It).
The labels WITH distances are available [here](https://owncloud.cesnet.cz/index.php/s/XUpSf9cXOvRoSay).

## Training

Training is executed using the `train.py` file with command-line arguments. The listing of the available arguments is in the file `train.py`, an example may look like

```
train.py
  --model_type yolo3_xception
  --model_image_size 608x608
  --annotation_file yolo3_labels/with_dist/all_except_nodata_train.txt
  --anchors_path configs/yolo3_anchors.txt
  --freeze_level 0
  --classes_path configs/kitty_all_except_nodata.txt
  --total_epoch 200
  --batch_size 2
```

**Note:** High learning rate at the beginning of the training process may lead into gradient explosion and `nan` consequently. Use low learning 
rates (e.g., 1e-6) at the beginning of the training process with random initial weights. 

## Dumping the model

Note that the trained model must be dumped first for the predictions. Dumping is done via `yolo.py` script with the 
command line arguments (note the argument `dump_model`). The example of the dump arguments follows:

```
yolo.py
  --model_type yolo3_xception
  --model_image_size 608x608
  --weights_path trained_final.h5
  --anchors_path configs/yolo3_anchors.txt
  --classes_path configs/kitty_car_truck.txt
  --dump_model
  --output_model_file trained_final_dumped.h5
```

## Evaluation

**Note:** Note that evaluation must be done on the dumped model (see above).

Evaluation is done via `eval.py` script with command line arguments (see for all the arguments the file). An example of the evaluation is:

 ```
eval.py
  --model_path trained_final_dumped.h5
  --anchors_path configs/yolo3_anchors.txt
  --classes_path configs/kitty_all_except_nodata.txt
  --model_image_size 608x608
  --eval_type VOC
  --iou_threshold 0.5
  --conf_threshold 0.001
  --annotation_file yolo3_labels\with_dist\all_except_nodata_test.txt
  --save_result
```

## Pretrained model
Currently, only one partially pretrained model is publicly available. The model was 
trained for 43 epochs with loss 23.239 and val_loss 24.000 over the Kitty dataset 
mentioned above. The original model is available [here](https://owncloud.cesnet.cz/index.php/s/eRSgmQnRR15pShS), 
the dumped version is available [here](https://owncloud.cesnet.cz/index.php/s/q7ZzfEy5wUplrDm).
