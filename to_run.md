# Training scripts
## ???


## Kitty - all (except nodata)
### Train

```
python train.py 
  --model_type yolo3_xception 
  --model_image_size  608x608 
  --annotation_file "D:\DataSets\kitty\yolo3_labels\all_except_nodata_train.txt" 
  --anchors_path configs/yolo3_anchors.txt 
  --classes_path configs/kitty_all_except_nodata.txt
  --freeze_level 0  
  --total_epoch 200 
  --batch_size 2         
```

### Dump
```
python yolo.py 
  --model_type yolo3_xception 
  --model_image_size 608x608 
  --weights_path D:\keras-YOLOv3-model-set\logs\yolo_original_kitty_all_except_nodata\trained_final.h5 
  --anchors_path configs/yolo3_anchors.txt 
  --classes_path configs/kitty_all_except_nodata.txt 
  --dump_model 
  --output_model_file=D:\keras-YOLOv3-model-set\logs\yolo_original_kitty_all_except_nodata\trained_final_dumped.h5

```

### Inference

```
python eval.py 
  --model_path D:\keras-YOLOv3-model-set\logs\yolo_original_kitty_all_except_nodata\trained_final_dumped.h5 
  --anchors_path configs/yolo3_anchors.txt 
  --classes_path configs/kitty_all_except_nodata.txt 
  --model_image_size 608x608 
  --eval_type=VOC  
  --iou_threshold=0.5 
  --conf_threshold=0.001 
  --annotation_file "D:\DataSets\kitty\yolo3_labels\all_except_nodata_test.txt" 
  --save_result
```